# elvah Smart Pricing SDK

1. [Installation](#installation)
	- [Swift Package Manager](#swift-package-manager)
	- [CocoaPods](#cocoapods)
2. [Getting Started](#getting-started)
	- [Safe Area Padding](#safe-area-padding)
	- [Mocking Data](#mocking-data)
3. [Examples](#examples)
4. [License](#license)

## Installation

The elvah Smart Pricing SDK supports iOS 14+ and requires Swift 5.10 to compile. You can install it either through the Swift Package Manager or through CocoaPods.

### Swift Package Manager

Add the following line to the dependencies in your `Package.swift` file:

```swift
.package(url: "https://gitlab.com/kreait/elvah/elvah-smart-pricing-sdk-ios", from: "0.1.0")
```

Alternatively, if you want to add the package to your Xcode project, go to `File` > `Add Packages...` and enter the URL "https://gitlab.com/kreait/elvah/elvah-smart-pricing-sdk-ios" into the search field at the top right. The package should appear in the list. Select it and click "Add Package" in the bottom right.

### CocoaPods

In your `Podfile`, add the following line:

```
pod "ElvahSmartPricing", :git => "https://gitlab.com/kreait/elvah/elvah-smart-pricing-sdk-ios", :tag => '0.1.0'
```

Then run `pod install` and you are good to go!

> Note: Please make sure to include `use_frameworks!` in the app target of your Podfile. Otherwise, the SDK will not be able to include the required resources like fonts, assets and localization data. 

#### Sandboxing

Depending on your project setup, you might see an error when trying to build the project. It could look like this:

> Error: Sandbox: rsync.samba(95743) deny(1) file-read-data (...)/Frameworks/ElvahSmartPricing.framework/ElvahSmartPricing.bundle

This error is due to Xcode's default sandboxing behavior that must be turned off to use CocoaPods. You can do that by going to your target's build settings and setting `ENABLE_USER_SCRIPT_SANDBOXING` to `NO`. Your project should then build again.

## Getting Started

The elvah Smart Pricing SDK exposes a single SwiftUI view called `SmartPricingView` that takes an array of evse ids as its input:

```swift
SmartPricingView(evseIds: ["DE*test*0001", "DE*test*0002"])
```

> Note: The `SmartPricingView` is only able to fetch pricing data for one of the supported CPOs. Any unsupported evse ids passed to the view will be ignored by the backend and not displayed.

> Note: At this time, the SDK is in pre-release and does not have access to live data. Please enable the simulated mode on the view to test your implementation. See more in [Mocking Data](#mocking-data).

### Safe Area Padding

The `SmartPricingView` contains an edge-to-edge scrollview, so you should not apply a `.padding()` directly to the view. Due to the lack of the `safeAreaPadding` modifier for scroll views in iOS 14, instead, if you need to apply horizontal padding, you can pass it via an additional parameter:

```swift
SmartPricingView(evseIds: ["DE*test*0001", "DE*test*0002"], safeAreaPadding: 20)
```

The default value is `nil`, which will apply the system default.


### Item Spacing

If you want to specify the spacing between the individual items inside the `SmartPricingView`, you can pass an additional value to the view:

```swift
SmartPricingView(evseIds: ["DE*test*0001", "DE*test*0002"], itemSpacing: 10)
```

The default value is `nil`, which will apply the system default.

### Mocking Data

You can use the `SmartPricingView` in a simulated mode that will use mocked pricing information. To do that, simply add the `withMockedData(simulatesError:)` view modifier to the view.

```swift
SmartPricingView(evseIds: [])
  .withMockedData(simulatesError: false)
```

> Note: In the simulated mode, it doesn't matter which evse ids you pass to the view. They will be replaced with a set of mock evse ids.

## Examples

You can find an example project in the `Examples` directory of this repository.

## License

Copyright 2024 elvah GmbH

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
