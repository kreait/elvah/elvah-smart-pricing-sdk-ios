// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "elvah-smart-pricing-sdk-ios",
	defaultLocalization: "en",
	platforms: [.iOS(.v14)],
	products: [
		.library(
			name: "ElvahSmartPricing",
			targets: ["ElvahSmartPricing"]
		),
	],
	targets: [
		.target(
			name: "ElvahSmartPricing",
			resources: [
				.copy("Resources/Colors.xcassets"),
				.copy("Resources/Images.xcassets"),
				.copy("Resources/Localizable.xcstrings"),
				.copy("Resources/Kreon.ttf"),
				.copy("Resources/Quicksand.ttf")
			],
			swiftSettings: [
				.enableExperimentalFeature("StrictConcurrency"),
			]
		),
	],
	swiftLanguageVersions: [.v5]
)
