Pod::Spec.new do |s|
  s.name                  = 'ElvahSmartPricing'
  s.version               = '0.1.0'
  s.summary               = 'elvah Smart Pricing SDK'
  s.homepage              = 'https://www.elvah.de/'
  s.license               = { :type => 'MIT', :file => 'LICENSE.md' }
  s.author                = { 'elvah' => 'contact@elvah.de' }
  s.source                = { :git => 'https://gitlab.com/kreait/elvah/elvah-smart-pricing-sdk-ios', :tag => s.version.to_s }
  s.ios.deployment_target = '14.0'
  s.swift_version         = '5.10'
  s.source_files          = 'Sources/ElvahSmartPricing/**/*.{swift,plist}'
  s.resource_bundle       = { 'ElvahSmartPricing' => 'Sources/ElvahSmartPricing/Resources/**/*.{xcstrings,strings,xcassets,ttf}' }
end
