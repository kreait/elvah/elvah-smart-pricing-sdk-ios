// Copyright © elvah. All rights reserved.

import SwiftUI

/// A type describing a charge point.
struct Chargepoint: Identifiable, Hashable, Sendable {
	/// The identifier of the charge point.
	var id: String { evseId }

	/// The identifier of the charge point.
	var evseId: String

	/// The charge point's physical reference, if available.
	var physicalReference: String?

	/// The charging  price per kWh.
	var pricePerKWH: Currency

	/// The maximum power of the charge point in kW.
	var maxPowerInKw: Double

	/// The charge point's availability.
	var availability: Availability

	/// The date at which the charge point's availability was last updated.
	var availabilityUpdatedAt: Date

	/// The charge point's connectors.
	var connectors: Set<Connector>
}

extension Chargepoint {
	/// The url to the web app that can start a charging process at this chargepoint.
	var chargeUrl: URL? {
		let languageCode = Locale.current.languageCode ?? "de"
		// Fixed demo evse id for now
		// TODO: Replace with self.evseId for release
		let urlString = """
		https://direct-charge.int.ecm.energy/?evseId=DE*SIM*E000001*1&lang=\(languageCode)
		"""
		return URL(string: urlString)?.properlyEncoded()
	}
}

extension Chargepoint {
	/// The charge point's availability.
	enum Availability: Hashable, Sendable {
		case available
		case unavailable
		case outOfService
	}

	/// A charge point's connector.
	enum Connector: Hashable, Sendable {
		case chademo
		case combo
		case type2
		case other
	}
}

// MARK: - Helpers

extension Chargepoint {
	/// The maximum power of the charge point in kW, formatted to be used in a user-facing string.
	var maxPowerInKwFormatted: String {
		"\(Int(maxPowerInKw))"
	}

	/// Returns a flag determining if the chargepoint's availability is set to `unavailable`.
	var isOccupied: Bool {
		availability == .unavailable
	}

	/// Returns a flag determining if the chargepoint's availability is set to `outOfService`.
	var isOutOfService: Bool {
		availability == .outOfService
	}

	/// Returns a user-facing label that can be used to describe the chargepoint's availability.
	var availabilityDisplayName: LocalizedStringKey {
		if isOccupied {
			return "Occupied"
		}

		if isOutOfService {
			return "Out of service"
		}

		return "Available"
	}

	var availabilityForegroundColor: Color {
		if isOccupied {
			return .yellow
		}

		if isOutOfService {
			return .red
		}

		return .primary
	}
}

// MARK: - Assets

extension Chargepoint.Connector {
	/// The asset belonging to the connector.
	var assetName: String {
		switch self {
		case .chademo:
			"connector.chademo.filled"
		case .combo:
			"connector.combo.filled"
		case .other:
			"connector.other.filled"
		case .type2:
			"connector.other.filled"
		}
	}
}
