// Copyright © elvah. All rights reserved.

import Foundation

/// An enumeration representing the possible states of a loadable resource.
public enum LoadableState<Value> {
	/// Represents the state where the resource has not yet been requested or is missing.
	case absent

	/// Represents the state where the resource is currently being loaded.
	case loading

	/// Represents the state where an error occurred during the loading process.
	///
	/// - Parameter Error: The error that occurred while attempting to load the resource.
	case error(Error)

	/// Represents the state where the resource has been successfully loaded.
	///
	/// - Parameter Value: The successfully loaded resource.
	case loaded(Value)
}

/// An extension providing utility methods for manipulating and querying the state of
/// `LoadableState`.
public extension LoadableState {
	/// Sets the state to `absent`.
	mutating func setAbsent() {
		self = .absent
	}

	/// Sets the state to `loading`.
	mutating func setLoading() {
		self = .loading
	}

	/// Sets the state to `error` with the given error payload.
	///
	/// - Parameter error: The error that occurred while attempting to load the resource.
	mutating func setError(_ error: Swift.Error) {
		self = .error(error)
	}

	/// Sets the state to `loaded` with the given value payload.
	///
	/// - Parameter value: The successfully loaded resource.
	mutating func setValue(_ value: Value) {
		self = .loaded(value)
	}

	// MARK: - Convenience Methods

	/// A Boolean value indicating whether the state is `absent`.
	var isAbsent: Bool {
		if case .absent = self {
			return true
		}
		return false
	}

	/// A Boolean value indicating whether the state is `loading`.
	var isLoading: Bool {
		if case .loading = self {
			return true
		}
		return false
	}

	/// A Boolean value indicating whether the state is `error`.
	var isError: Bool {
		if case .error = self {
			return true
		}
		return false
	}

	/// A Boolean value indicating whether the state is `loaded`.
	var isLoaded: Bool {
		if case .loaded = self {
			return true
		}
		return false
	}

	/// The error payload if the state is `error`, otherwise `nil`.
	var error: Swift.Error? {
		if case let .error(error) = self {
			return error
		}
		return nil
	}

	/// The value payload if the state is `loaded`, otherwise `nil`.
	var data: Value? {
		if case let .loaded(data) = self {
			return data
		}
		return nil
	}

	// MARK: - Map

	/// Transforms the current `LoadableState` with a specified value type to a new `LoadableState`
	/// with a different value type.
	///
	/// This method applies a transformation function to the loaded value of the current state,
	/// if available, and returns a new `LoadableState` instance with that transformed value.
	/// If the current state is `absent`, `loading`, or `error`, it returns the same state
	/// without applying the transformation.
	///
	/// - Parameter transform: A closure that takes the current value of type `Value` and returns a
	/// new value of type `T`. The closure can throw an error, in which case the method will rethrow
	/// it.
	/// - Returns: A new `LoadableState` instance with the transformed value of type `T`,
	/// or the same state if the current state is `absent`, `loading`, or `error`.
	/// - Throws: Rethrows any error that the `transform` closure might throw.
	func map<T>(_ transform: (Value) throws -> T) rethrows -> LoadableState<T> {
		return switch self {
		case .absent: .absent
		case .loading: .loading
		case let .error(error): .error(error)
		case let .loaded(data): try .loaded(transform(data))
		}
	}

	// MARK: - compactMap

	/// Transforms the current `LoadableState` with a specified value type to a new `LoadableState`
	/// with a different value type.
	///
	/// This method applies a transformation function to the loaded value of the current state,
	/// if available, and returns a new `LoadableState` instance with that transformed value.
	/// If the current state is `absent`, `loading`, or `error`, it returns the same state
	/// without applying the transformation.
	///
	/// - Parameter transform: A closure that takes the current value of type `Value` and returns a
	/// new value of type `T?`. If the result of the transform is `nil`, then the given`emptyState`
	/// will be returned as resulting state. The closure can throw an error, in which case the method
	/// will rethrow it.
	/// - Parameter emptyState: The `LoadableState` to set the value to if the transformed value is
	/// `nil`. Defaults to `.absent`.
	/// - Returns: A new `LoadableState` instance with the transformed value of type `T`,
	/// or the same state if the current state is `absent`, `loading`, or `error`.
	/// - Throws: Rethrows any error that the `transform` closure might throw.
	func compactMap<T>(
		emptyState: LoadableState<T> = .absent,
		_ transform: (Value) throws -> T?
	) rethrows -> LoadableState<T> {
		switch self {
		case .absent: return .absent
		case .loading: return .loading
		case let .error(error): return .error(error)
		case let .loaded(value):
			if let transformed = try transform(value) {
				return .loaded(transformed)
			}
			return emptyState
		}
	}
}

extension LoadableState: Sendable where Value: Sendable {}
extension LoadableState: Equatable where Value: Equatable {
	public nonisolated static func == (
		lhs: LoadableState,
		rhs: LoadableState
	) -> Bool {
		switch (lhs, rhs) {
		case (.absent, .absent): return true
		case (.loading, .loading): return true
		case (.error, .error): return true
		case let (.loaded(leftData), .loaded(rightData)): return leftData == rightData
		default: return false
		}
	}
}
