// Copyright © elvah. All rights reserved.

import Foundation

// MARK: - EvseIds

let mockEvseIds = [
	Chargepoint.mockAvailable.evseId,
	Chargepoint.mockUnavailable.evseId,
	Chargepoint.mockOutOfService.evseId
]

// MARK: - Chargepoints

extension Chargepoint {
	static func mockLoading(evseId: String) -> Chargepoint {
		Chargepoint(
			evseId: evseId,
			physicalReference: nil,
			pricePerKWH: .init(0.54),
			maxPowerInKw: 150,
			availability: .available,
			availabilityUpdatedAt: Date().addingTimeInterval(-100_000),
			connectors: [.chademo]
		)
	}

	static let mockAvailable = Chargepoint(
		evseId: "ABCDEFG*1234",
		physicalReference: nil,
		pricePerKWH: .init(0.52),
		maxPowerInKw: 150,
		availability: .available,
		availabilityUpdatedAt: Date().addingTimeInterval(-100_000),
		connectors: [.chademo]
	)

	static let mockUnavailable = Chargepoint(
		evseId: "ABCDEFG*1235",
		physicalReference: nil,
		pricePerKWH: .init(0.63),
		maxPowerInKw: 20,
		availability: .unavailable,
		availabilityUpdatedAt: Date().addingTimeInterval(-200_000),
		connectors: [.combo]
	)

	static let mockOutOfService = Chargepoint(
		evseId: "ABCDEFG*1236",
		physicalReference: nil,
		pricePerKWH: .init(0.67),
		maxPowerInKw: 350,
		availability: .outOfService,
		availabilityUpdatedAt: Date().addingTimeInterval(-300_000),
		connectors: [.chademo]
	)
}

// MARK: - Historic Prices

extension [HistoricPrice] {
	static let pastMonth: Self = [
		.init(date: .daysInPast(30), price: 0.52),
		.init(date: .daysInPast(29), price: 0.54),
		.init(date: .daysInPast(28), price: 0.56),
		.init(date: .daysInPast(27), price: 0.68),
		.init(date: .daysInPast(26), price: 0.59),
		.init(date: .daysInPast(25), price: 0.60),
		.init(date: .daysInPast(24), price: 0.62),
		.init(date: .daysInPast(23), price: 0.67),
		.init(date: .daysInPast(22), price: 0.57),
		.init(date: .daysInPast(21), price: 0.56),
		.init(date: .daysInPast(20), price: 0.52),
		.init(date: .daysInPast(19), price: 0.62),
		.init(date: .daysInPast(18), price: 0.58),
		.init(date: .daysInPast(17), price: 0.57),
		.init(date: .daysInPast(16), price: 0.55),
		.init(date: .daysInPast(15), price: 0.54),
		.init(date: .daysInPast(14), price: 0.56),
		.init(date: .daysInPast(12), price: 0.57),
		.init(date: .daysInPast(11), price: 0.67),
		.init(date: .daysInPast(10), price: 0.59),
		.init(date: .daysInPast(9), price: 0.63),
		.init(date: .daysInPast(8), price: 0.72),
		.init(date: .daysInPast(7), price: 0.65),
		.init(date: .daysInPast(6), price: 0.68),
		.init(date: .daysInPast(5), price: 0.67),
		.init(date: .daysInPast(4), price: 0.63),
		.init(date: .daysInPast(3), price: 0.66),
		.init(date: .daysInPast(2), price: 0.61),
		.init(date: .daysInPast(1), price: 0.63),
		.init(date: .daysInPast(0), price: 0.62),
	]
}
