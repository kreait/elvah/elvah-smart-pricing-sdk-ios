// Copyright © elvah. All rights reserved.

import SwiftUI

/// An enum containing the names of the fonts that this SDK uses.
enum SDKFont: String, CaseIterable, Sendable {
	case kreon = "Kreon"
	case quicksand = "Quicksand"

	var fileName: String {
		switch self {
		case .kreon:
			"Kreon.ttf"
		case .quicksand:
			"Quicksand.ttf"
		}
	}
}
