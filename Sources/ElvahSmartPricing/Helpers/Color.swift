// Copyright © elvah. All rights reserved.

import SwiftUI

extension Color {
	/// The primary accented foreground color used in the components of this package.
	static let accentedPrimary = Color("primaryBlue", bundle: SDK.bundle)

	/// The primary accented background color used in the components of this package.
	static let accentedBackground = Color("secondaryBlue", bundle: SDK.bundle)

	/// The primary accented gray color used in the components of this package.
	static let accentedGray = Color("primaryGray", bundle: SDK.bundle)
}
