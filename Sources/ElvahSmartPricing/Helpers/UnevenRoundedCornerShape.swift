// Copyright © elvah. All rights reserved.

import SwiftUI

/// A shape that supports rounding specific corners individually.
struct UnevenRoundedCornerShape: Shape {
	/// The corner radius.
	var radius: CGFloat
	
	/// The corners to round.
	var corners: UIRectCorner = .allCorners

	func path(in rect: CGRect) -> Path {
		let path = UIBezierPath(
			roundedRect: rect,
			byRoundingCorners: corners,
			cornerRadii: CGSize(width: radius, height: radius)
		)
		return Path(path.cgPath)
	}
}

extension View {
	/// Returns a view that has the specified corners rounded to the given `radius`.
	/// - Parameters:
	///   - radius: The corner radius.
	///   - corners: The corners to round.
	/// - Returns: Returns a view that has the specified corners rounded to the given `radius`.
	func unevenCornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
		clipShape(UnevenRoundedCornerShape(radius: radius, corners: corners))
	}
}
