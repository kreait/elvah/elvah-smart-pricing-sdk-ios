// Copyright © elvah. All rights reserved.

import SwiftUI

/// A helper view modifier that logs a warning if the provided evse ids array is or becomes empty
/// during the lifetime of the view.
private struct ArrayNotEmptyAssertionModifier: ViewModifier {
	var evseIds: [String]

	func body(content: Content) -> some View {
		content
			.onAppear {
				if evseIds.isEmpty {
					logViolation()
				}
			}
			.onChange(of: evseIds) { evseIds in
				if evseIds.isEmpty {
					logViolation()
				}
			}
	}

	private func logViolation() {
		SDK.logger.warning(
			"""
			You are using SmartPricingView with an empty array of evse ids. This is not supported.
			"""
		)
	}
}

extension View {
	func assertNonEmptyEvseIds(evseIds: [String]) -> some View {
		modifier(ArrayNotEmptyAssertionModifier(evseIds: evseIds))
	}
}
