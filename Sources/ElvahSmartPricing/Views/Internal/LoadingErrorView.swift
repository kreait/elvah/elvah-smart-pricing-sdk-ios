// Copyright © elvah. All rights reserved.

import SwiftUI

struct LoadingErrorView: View {
	@Environment(\.componentInset) private var componentInset
	@Environment(\.font) private var font

	var body: some View {
		AccentedCard {
			VStack(spacing: 20) {
				Image(systemName: "exclamationmark.triangle.fill")
					.resizable()
					.aspectRatio(contentMode: .fit)
					.frame(width: 35, height: 35)
					.foregroundColor(Color.accentedPrimary)
				VStack(spacing: 10) {
					Text("Could not find prices\nfor the station", bundle: .automatic)
						.font(.quicksand(size: 18))
						.fontWeight(.bold)
						.fixedSize(horizontal: false, vertical: true)
					Text("Prices are not available at the moment", bundle: .automatic)
						.font(.quicksand(size: 13))
						.fontWeight(.bold)
						.foregroundColor(Color.accentedPrimary)
						.fixedSize(horizontal: false, vertical: true)
				}
			}
			.multilineTextAlignment(.center)
			.padding(25)
			.frame(maxWidth: .infinity)
		}
	}
}

#Preview("Light") {
	LoadingErrorView()
		.padding()
		.withFontRegistration()
}

#Preview("Dark") {
	LoadingErrorView()
		.padding()
		.preferredColorScheme(.dark)
		.withFontRegistration()
}
