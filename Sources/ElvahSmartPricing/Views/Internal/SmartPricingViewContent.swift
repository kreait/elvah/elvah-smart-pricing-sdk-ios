// Copyright © elvah. All rights reserved.

import SwiftUI

@MainActor
struct SmartPricingViewContent: View {
	@EnvironmentObject private var provider: DataProvider
	@Environment(\.componentInset) private var componentInset
	@Environment(\.itemSpacing) private var itemSpacing

	/// The fetched chargpoints.
	@State private var chargepoints: LoadableState<[String: Chargepoint]> = .loading

	/// The fetched data points for the price history.
	@State private var priceHistory: LoadableState<PriceHistory> = .loading

	/// The cheapest chargepoint today, from the list of available evse ids.
	@State private var currentCheapestPrice: LoadableState<Currency> = .loading

	/// The task responsible for loading the view's data.
	@State private var loadingTask: Task<Void, Never>?

	/// A helper state indicating the absence of required data due to an error in the loading process.
	@State private var hasFailedToFetchRequiredData = false

	/// The list of evseIds that the smart pricing view should show prices for.
	private var evseIds: [String]

	/// If the chargepoints have been loaded, this will return all evse ids that have a matching
	/// chargepoint.
	/// If the chargepoints have not been loaded yet, this will return all evse ids.
	private var availableEvseIds: [String] {
		if let chargepoints = chargepoints.data {
			return evseIds.filter { chargepoints.keys.contains($0) }
		}

		return evseIds
	}

	/// Initializes the view.
	init(evseIds: [String]) {
		self.evseIds = evseIds
	}

	var body: some View {
		VStack(spacing: itemSpacing) {
			if hasFailedToFetchRequiredData {
				LoadingErrorView()
					.padding(.horizontal, componentInset)
			} else {
				VStack {
					PricingHeader(currentCheapestPrice: currentCheapestPrice)
					if #available(iOS 16.0, *), priceHistory.isError == false {
						PriceHistoryView(priceHistory: priceHistory)
							.padding(.horizontal, componentInset)
					}
				}
				ScrollView(.horizontal) {
					HStack(spacing: itemSpacing) {
						ForEach(availableEvseIds, id: \.self) { evseId in
							let chargepoint = chargepoints.compactMap { $0[evseId] }
							Button {
								if let chargepoint = chargepoint.data {
									openChargepoint(chargepoint)
								}
							} label: {
								ChargepointView(evseId: evseId, chargepoint: chargepoint)
							}
							.buttonStyle(.plain)
						}
					}
					.padding(.horizontal, componentInset)
				}
			}
		}
		.animation(.default, value: chargepoints)
		.animation(.default, value: priceHistory)
		.onAppear {
			startLoadingTask()
		}
		.onChange(of: evseIds) { evseIds in
			resetData()
			startLoadingTask()
		}
		.onDisappear {
			stopLoadingTask()
		}
	}

	// MARK: - Helpers

	private func startLoadingTask() {
		loadingTask = Task { [evseIds] in
			async let priceHistory = provider.priceHistory(evseIds: evseIds)
			async let chargepoints = provider.chargepoints(evseIds: evseIds)

			do {
				self.priceHistory = try await .loaded(priceHistory)
			} catch {
				self.priceHistory.setError(error)
			}

			do {
				self.chargepoints = try await .loaded(chargepoints)
			} catch {
				self.chargepoints.setError(error)
			}

			do {
				if let priceHistory = self.priceHistory.data {
					self.currentCheapestPrice = .loaded(priceHistory.currentCheapestPrice)
				} else {
					// Fallback
					self.currentCheapestPrice = try await .loaded(findCheapestPrice(in: chargepoints.values))
				}
			} catch {
				self.currentCheapestPrice.setError(error)
			}

			withAnimation {
				// We ignore failures for the price history and simply don't show the chart in that case
				hasFailedToFetchRequiredData = self.chargepoints.isError
			}
		}
	}

	private func findCheapestPrice(in chargepoints: some Sequence<Chargepoint>) throws -> Currency {
		let cheapestPrice = chargepoints.map(\.pricePerKWH).min()
		guard let cheapestPrice else {
			throw SmartPricingError.fetchedDataIsEmpty
		}

		return cheapestPrice
	}

	private func resetData() {
		stopLoadingTask()
		hasFailedToFetchRequiredData = false
		priceHistory.setLoading()
		chargepoints.setLoading()
	}

	private func stopLoadingTask() {
		loadingTask?.cancel()
	}

	private func openChargepoint(_ chargepoint: Chargepoint) {
		guard let url = chargepoint.chargeUrl, UIApplication.shared.canOpenURL(url) else {
			return
		}

		UIApplication.shared.open(url)
	}
}

#Preview("Light") {
	ScrollView {
		SmartPricingViewContent(evseIds: mockEvseIds)
			.environmentObject(DataProvider.mock)
	}
	.withFontRegistration()
}

#Preview("Dark") {
	ScrollView {
		SmartPricingViewContent(evseIds: mockEvseIds)
			.environmentObject(DataProvider.mock)
	}
	.preferredColorScheme(.dark)
	.withFontRegistration()
}

#Preview("Partial Error") {
	ScrollView {
		SmartPricingViewContent(evseIds: mockEvseIds)
			.environmentObject(DataProvider.mockPriceHistoryError)
	}
	.withFontRegistration()
}

#Preview("Full Error") {
	ScrollView {
		SmartPricingViewContent(evseIds: mockEvseIds)
			.environmentObject(DataProvider.mockAllError)
	}
	.withFontRegistration()
}
