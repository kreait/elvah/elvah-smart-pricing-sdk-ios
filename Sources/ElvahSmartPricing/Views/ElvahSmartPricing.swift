// Copyright © elvah. All rights reserved.

import SwiftUI

/// A view that highlights the price history of a set of charge points and provides links
/// for users to start a charging process at one of the available charge points.
///
/// The view will automatically start fetching required data and handles loading and failure
/// states automatically as well.
///
/// - Important: The provided list of evse ids must not contain duplicate entries.
@MainActor
public struct SmartPricingView: View {
	/// The list of evse ids that the smart pricing view should show prices for.
	private var evseIds: [String]

	/// The horizontal padding that will be applied to the content.
	private var safeAreaPadding: CGFloat?

	/// The spacing between items that will be applied to the content.
	private var itemSpacing: CGFloat?

	/// A value indicating if the view should use mocked data or real data.
	///
	/// Defaults to `false`.
	private var isMocked = false

	/// A flag indicating if a loading error should be simulated.
	///
	/// Defaults to `false`.
	private var simulatesError = false

	/// A view that highlights the price history of a set of charge points and provides links
	/// for users to start a charging process at one of the available charge points.
	///
	/// The view will automatically start fetching required data and handles loading and failure
	/// states automatically as well.
	///
	/// - Important: The provided list of evse ids must not contain duplicate entries.
	///
	/// - Parameter evseIds: The list of evse ids that the smart pricing view should show prices for.
	/// - Parameter safeAreaPadding: The horizontal padding that will be applied to the content. If
	/// set to `nil`, the system's default value will be used. Defaults to `nil`.
	/// - Parameter itemSpacing: The spacing between items that be applied to the content. If
	/// set to `nil`, the system's default value will be used. Defaults to `nil`.
	public init(evseIds: [String], safeAreaPadding: CGFloat? = nil, itemSpacing: CGFloat? = nil) {
		SDK.registerFonts()
		self.evseIds = evseIds
		self.safeAreaPadding = safeAreaPadding
		self.itemSpacing = itemSpacing
	}

	public var body: some View {
		SmartPricingViewContent(evseIds: evseIds)
			.componentInset(safeAreaPadding)
			.itemSpacing(itemSpacing)
			.environmentObject(dataProvider)
			.assertNonEmptyEvseIds(evseIds: evseIds)
	}

	/// Modifies the smart pricing view so that it will use mocked data for testing purposes.
	/// This will replace the evse ids you passed with a set of mock evse ids.
	/// - Parameter simulatesError: A flag indicating if a loading error should be simulated. Defaults
	/// to `nil`.
	/// - Returns: A modified smart pricing view that uses mocked data.
	public func withMockedData(simulatesError: Bool = false) -> some View {
		var copy = self
		copy.evseIds = mockEvseIds
		copy.isMocked = true
		copy.simulatesError = simulatesError
		return copy
	}

	// MARK: - Helpers

	/// The data provider that should be used by the view.
	private var dataProvider: DataProvider {
		if isMocked && simulatesError {
			return .mockAllError
		}
		if isMocked {
			return .mock
		}
		return .live
	}
}

#Preview {
	ScrollView {
		SmartPricingView(evseIds: mockEvseIds)
			.withMockedData(simulatesError: false)
	}
}
