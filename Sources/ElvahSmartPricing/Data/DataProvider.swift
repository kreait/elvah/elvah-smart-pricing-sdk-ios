// Copyright © elvah. All rights reserved.

import SwiftUI

/// The data provider is an injectable model object for view. It is responsible for providing and
/// abstract way of requesting external data to the views.
@MainActor
final class DataProvider: ObservableObject {
	struct Dependencies: Sendable {
		var chargepoints: @Sendable (_ evseIds: [String]) async throws -> [String: Chargepoint]
		var priceHistory: @Sendable (_ evseIds: [String]) async throws -> PriceHistory
	}

	private let dependencies: Dependencies

	/// The data provider is an injectable model object for view. It is responsible for providing and
	/// abstract way of requesting external data to the views.
	nonisolated init(dependencies: Dependencies) {
		self.dependencies = dependencies
	}

	/// Fetches the chargepoint data for the given evseIds.
	/// - Parameter evseIds: The evseIds.
	/// - Returns: The chargepoint data.
	func chargepoints(evseIds: [String]) async throws -> [String: Chargepoint] {
		try await dependencies.chargepoints(evseIds)
	}

	/// Fetches the price history for the given evseIds.
	/// - Parameter evseIds: The evseIds.
	/// - Returns: The price history.
	func priceHistory(evseIds: [String]) async throws -> PriceHistory {
		try await dependencies.priceHistory(evseIds)
	}
}

// MARK: - Instances

extension DataProvider {
	nonisolated static let live: DataProvider = {
		let loader = DataLoader()
		return .init(
			dependencies: .init(
				chargepoints: { evseIds in
					try await loader.chargepoints(evseIds: evseIds)
				},
				priceHistory: { evseIds in
					try await loader.priceHistory(evseIds: evseIds)
				}
			)
		)
	}()

	nonisolated static let mock: DataProvider = .init(
		dependencies: .init(
			chargepoints: { evseIds in
				try await Task.sleep(nanoseconds: 800 * NSEC_PER_MSEC)
				return [
					"ABCDEFG*1234": .mockAvailable,
					"ABCDEFG*1235": .mockUnavailable,
					"ABCDEFG*1236": .mockOutOfService,
				]
			},
			priceHistory: { evseIds in
				try await Task.sleep(nanoseconds: 800 * NSEC_PER_MSEC)
				return try .init(
					values: .pastMonth,
					currentCheapestPrice: [HistoricPrice].pastMonth.map(\.price).min()!
				)
			}
		)
	)

	nonisolated static let mockPriceHistoryError: DataProvider = .init(
		dependencies: .init(
			chargepoints: { evseIds in
				try await Task.sleep(nanoseconds: 800 * NSEC_PER_MSEC)
				return [
					"ABCDEFG*1234": .mockAvailable,
					"ABCDEFG*1235": .mockUnavailable,
					"ABCDEFG*1236": .mockOutOfService,
				]
			},
			priceHistory: { evseIds in
				try await Task.sleep(nanoseconds: 800 * NSEC_PER_MSEC)
				throw SmartPricingError.unknown
			}
		)
	)

	nonisolated static let mockAllError: DataProvider = .init(
		dependencies: .init(
			chargepoints: { evseIds in
				try await Task.sleep(nanoseconds: 800 * NSEC_PER_MSEC)
				throw SmartPricingError.unknown
			},
			priceHistory: { evseIds in
				try await Task.sleep(nanoseconds: 800 * NSEC_PER_MSEC)
				throw SmartPricingError.unknown
			}
		)
	)
}
