// Copyright © elvah. All rights reserved.

/// An object responsible for fetching smart pricing data.
actor DataLoader {
	func chargepoints(evseIds: [String]) async throws -> [String: Chargepoint] {
		// TODO: Network call
		SDK.logger.debug("Chargepoint pricing data is not available. Please use the simulated mode.")
		throw SmartPricingError.unknown
	}

	func priceHistory(evseIds: [String]) async throws -> PriceHistory {
		// TODO: Network call
		SDK.logger.debug("Price history data is not available. Please use the simulated mode.")
		throw SmartPricingError.unknown
	}
}
