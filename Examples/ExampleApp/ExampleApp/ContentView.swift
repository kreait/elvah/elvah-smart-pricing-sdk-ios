// Copyright © elvah. All rights reserved.

import ElvahSmartPricing
import SwiftUI

struct ContentView: View {
	var body: some View {
		ScrollView {
			VStack(spacing: 20) {
				box(height: 40)
				SmartPricingView(evseIds: [], safeAreaPadding: 15, itemSpacing: 20)
					.withMockedData()
				box(height: 100)
				box(height: 200)
				box(height: 150)
			}
		}
	}

	@ViewBuilder private func box(height: CGFloat) -> some View {
		RoundedRectangle(cornerRadius: 10)
			.foregroundStyle(.background.secondary)
			.padding(.horizontal, 15)
			.frame(height: height)
	}
}

#Preview("Light - German") {
	ContentView()
		.environment(\.locale, .init(identifier: "de"))
}

#Preview("Light - English") {
	ContentView()
		.environment(\.locale, .init(identifier: "en"))
}

#Preview("Dark") {
	ContentView()
		.preferredColorScheme(.dark)
}
