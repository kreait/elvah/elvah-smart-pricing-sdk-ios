// Copyright © elvah. All rights reserved.

import SwiftUI

@main
struct ExampleApp: App {
	var body: some Scene {
		WindowGroup {
			ContentView()
		}
	}
}
