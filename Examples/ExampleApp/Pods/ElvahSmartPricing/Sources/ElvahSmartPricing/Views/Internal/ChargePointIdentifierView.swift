// Copyright © elvah. All rights reserved.

import SwiftUI

struct ChargePointIdentifierView: View {
	let evseId: String
	let physicalReference: String?

	var body: some View {
		HStack(spacing: 4) {
			if let physicalReference = physicalReference, physicalReference.isEmpty == false {
				Text(physicalReference)
					.foregroundColor(Color.accentedPrimary)
					.bold()
			} else {
				Text(evseId.dropLast(4))
					.lineLimit(1)
					.truncationMode(.middle)
				Text(evseId.suffix(4))
					.bold()
					.lineLimit(1)
					.foregroundColor(Color.accentedPrimary)
			}
		}
		.font(.quicksand(size: 12))
	}
}

#Preview {
	VStack {
		ChargePointIdentifierView(evseId: "SOME*EVSE*ID*0001", physicalReference: nil)
		ChargePointIdentifierView(evseId: "SOME*EVSE*ID*0001", physicalReference: "54")
	}
	.withFontRegistration()
}
