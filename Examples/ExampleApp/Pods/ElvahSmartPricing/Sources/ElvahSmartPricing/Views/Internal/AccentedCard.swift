// Copyright © elvah. All rights reserved.

import SwiftUI

struct AccentedCard<Content: View>: View {
	@Environment(\.colorScheme) private var colorScheme
	private let cornerRadius = 12.0

	var content: Content

	init(@ViewBuilder content: () -> Content) {
		self.content = content()
	}

	var body: some View {
		content
			.clipShape(RoundedRectangle(cornerRadius: cornerRadius))
			.background(backgroundContent)
			.overlay(outlineContent)
	}

	@ViewBuilder private var backgroundContent: some View {
		RoundedRectangle(cornerRadius: cornerRadius)
			.fill(Color.accentedBackground)
	}

	@ViewBuilder private var outlineContent: some View {
		switch colorScheme {
		case .dark:
			RoundedRectangle(cornerRadius: cornerRadius)
				.strokeBorder(Color.accentedPrimary, lineWidth: 2)
		case .light:
			EmptyView()
		@unknown default:
			EmptyView()
		}
	}
}

#Preview("Light") {
	AccentedCard {
		Text(verbatim: "Content")
			.padding()
	}
	.frame(width: 250, height: 250)
}

#Preview("Dark") {
	AccentedCard {
		Text(verbatim: "Content")
			.padding()
	}
	.frame(width: 250, height: 250)
	.preferredColorScheme(.dark)
}
