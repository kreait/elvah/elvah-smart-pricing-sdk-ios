// Copyright © elvah. All rights reserved.

import SwiftUI

struct PricingHeader: View {
	@Environment(\.sizeCategory) private var sizeCategory
	@Environment(\.componentInset) private var componentInset

	var currentCheapestPrice: LoadableState<Currency>

	var body: some View {
		AccessibilityBreakingHStack {
			content
		}
		.padding(.horizontal, componentInset)
		.transformEffect(.identity)
		.accessibilityElement(children: .ignore)
		.accessibilityLabel(localizedAccessibilityLabel)
		.accessibilityValue(localizedAccessibilityValue)
	}

	@ViewBuilder private var content: some View {
		Text("Current Price", bundle: .automatic)
			.font(.kreon(size: 16))
			.foregroundColor(Color.accentedGray)
		if sizeCategory < .accessibilityMedium {
			Spacer()
		}
		if #available(iOS 15.0, *) {
			priceLabel.monospacedDigit()
		} else {
			priceLabel
		}
	}

	@ViewBuilder private var priceLabel: some View {
		let price = Text(lowestPrice.formatted())
		let kWh = Text(" / kWh")
			.fontWeight(.regular)
			.foregroundColor(.white.opacity(0.8))
			.font(.quicksand(size: 10))

		Text("from \(price)\(kWh)", bundle: .automatic)
			.bold()
			.foregroundColor(.white)
			.font(.quicksand(size: 15))
			.padding(.horizontal, 8)
			.padding(.vertical, 4)
			.background(RoundedRectangle(cornerRadius: 5).fill(Color.accentedPrimary))
			.redacted(reason: isRedacted ? .placeholder : [])
	}

	private var isRedacted: Bool {
		currentCheapestPrice.isLoaded == false
	}

	private var lowestPrice: Currency {
		if let price = currentCheapestPrice.data {
			return price
		}

		// Data is loading or missing
		return .init(0.42)
	}

	// MARK: - Accessibility

	private var localizedAccessibilityLabel: String {
		NSLocalizedString("Current Price", bundle: .automatic, comment: "")
	}

	private var localizedAccessibilityValue: String {
		if let price = currentCheapestPrice.data {
			let key = "\(price.formatted()) per kilo watt hour"
			return NSLocalizedString(key, bundle: .automatic, comment: "")
		}

		let key = "No data available"
		return NSLocalizedString(key, bundle: .automatic, comment: "")
	}
}

#Preview("Light") {
	VStack {
		PricingHeader(currentCheapestPrice: .absent)
		PricingHeader(currentCheapestPrice: .loaded(.init(0.42)))
		PricingHeader(currentCheapestPrice: .error(SmartPricingError.fetchedDataIsEmpty))
	}
	.withFontRegistration()
}

#Preview("Dark") {
	VStack {
		PricingHeader(currentCheapestPrice: .absent)
		PricingHeader(currentCheapestPrice: .loaded(.init(0.42)))
		PricingHeader(currentCheapestPrice: .error(SmartPricingError.fetchedDataIsEmpty))
	}
	.withFontRegistration()
	.preferredColorScheme(.dark)
}
