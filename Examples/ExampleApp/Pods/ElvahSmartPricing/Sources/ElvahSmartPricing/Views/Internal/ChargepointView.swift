// Copyright © elvah. All rights reserved.

import SwiftUI

struct ChargepointView: View {
	var evseId: String

	var chargepoint: LoadableState<Chargepoint>

	var body: some View {
		AccentedCard {
			VStack(spacing: 8) {
				PriceBadge(label: resolvedChargepoint.pricePerKWH.formatted())
				ConnectorIcon(connector: resolvedChargepoint.connectors.first!)
				VStack(spacing: 4) {
					if chargepoint.isAbsent {
						AvailabilityLabel(label: "Loading failed")
							.foregroundColor(Color.red)
							.unredacted()
					} else {
						AvailabilityLabel(label: resolvedChargepoint.availabilityDisplayName)
							.foregroundColor(resolvedChargepoint.availabilityForegroundColor)
					}
					MaxPowerBadge(label: resolvedChargepoint.maxPowerInKwFormatted)
					ChargePointIdentifierView(
						evseId: evseId,
						physicalReference: resolvedChargepoint.physicalReference
					)
					.unredacted()
				}
			}
			.padding([.horizontal, .bottom])
			.frame(minWidth: 170, minHeight: 150)
		}
		.redacted(reason: isRedacted ? .placeholder : [])
		.contentShape(.rect)
		.transformEffect(.identity)
		.animation(.default, value: chargepoint)
		.accessibilityElement(children: .ignore)
		.accessibilityLabel(localizedAccessibilityLabel)
		.accessibilityValue(localizedAccessibilityValue)
	}

	private var isRedacted: Bool {
		chargepoint.isLoaded == false
	}

	/// The loaded chargepoint or mock to be used with redaction
	private var resolvedChargepoint: Chargepoint {
		if let chargepoint = chargepoint.data {
			return chargepoint
		}

		// Data is loading or missing
		return .mockLoading(evseId: evseId)
	}

	// MARK: - Accessibility

	private var localizedAccessibilityLabel: Text {
		Text("Charge point", bundle: .automatic)
	}

	private var localizedAccessibilityValue: Text {
		if let chargepoint = chargepoint.data {
			return Text(
				"""
				\(Text(chargepoint.availabilityDisplayName, bundle: .automatic)). \
				\(chargepoint.pricePerKWH.formatted()) per kilo watt hour
				""",
				bundle: .automatic
			)
		}
		return Text("No data available", bundle: .automatic)
	}
}

// MARK: - Helpers

private struct PriceBadge: View {
	var label: String

	var body: some View {
		Text(label)
			.font(.quicksand(size: 15))
			.bold()
			.foregroundColor(Color.white)
			.padding(.horizontal, 8)
			.padding(.vertical, 3)
			.frame(minWidth: 70)
			.background(backgroundContent)
	}

	@ViewBuilder private var backgroundContent: some View {
		Rectangle()
			.fill(Color.accentedPrimary)
			.unevenCornerRadius(10, corners: [.bottomLeft, .bottomRight])
	}
}

private struct ConnectorIcon: View {
	var connector: Chargepoint.Connector

	var body: some View {
		Image(connector.assetName, bundle: .automatic)
			.resizable()
			.aspectRatio(contentMode: .fit)
			.frame(height: 50)
	}
}

private struct AvailabilityLabel: View {
	var label: LocalizedStringKey
	var body: some View {
		Text(label, bundle: .automatic)
			.font(.quicksand(size: 12))
			.bold()
	}
}

private struct MaxPowerBadge: View {
	var label: String
	var body: some View {
		Text("\(label) kW", bundle: .automatic)
			.foregroundColor(.white)
			.font(.quicksand(size: 10))
			.bold()
			.padding(4)
			.background(RoundedRectangle(cornerRadius: 5).fill(Color.accentedPrimary))
	}
}

#Preview {
	VStack {
		ChargepointView(evseId: Chargepoint.mockAvailable.evseId, chargepoint: .loading)
		ChargepointView(evseId: Chargepoint.mockAvailable.evseId, chargepoint: .loaded(.mockAvailable))
		ChargepointView(
			evseId: Chargepoint.mockAvailable.evseId,
			chargepoint: .loaded(.mockUnavailable)
		)
		ChargepointView(evseId: Chargepoint.mockAvailable.evseId, chargepoint: .absent)
	}
	.withFontRegistration()
}
