// Copyright © elvah. All rights reserved.

import SwiftUI

struct AccessibilityBreakingHStack<Content: View>: View {
	var alignment: VerticalAlignment = .center
	var brokenAlignment: HorizontalAlignment = .center
	var spacing: CGFloat?
	@ViewBuilder var content: Content

	var body: some View {
		if #available(iOS 16, *) {
			_AccessibilityBreakingHStack(
				alignment: alignment,
				brokenAlignment: brokenAlignment,
				spacing: spacing
			) {
				content
			}
		} else {
			_AccessibilityBreakingHStackBackport(
				alignment: alignment,
				brokenAlignment: brokenAlignment,
				spacing: spacing
			) {
				content
			}
		}
	}
}

// MARK: - Implementation

@available(iOS 16.0, *)
private struct _AccessibilityBreakingHStack<Content: View>: View {
	@Environment(\.dynamicTypeSize) private var dynamicTypeSize

	var alignment: VerticalAlignment = .center
	var brokenAlignment: HorizontalAlignment = .center
	var spacing: CGFloat?
	@ViewBuilder var content: Content

	var body: some View {
		layout {
			content
		}
	}

	private var layout: AnyLayout {
		dynamicTypeSize >= .accessibility1
			? AnyLayout(VStackLayout(alignment: brokenAlignment, spacing: spacing))
			: AnyLayout(HStackLayout(alignment: alignment, spacing: spacing))
	}
}

private struct _AccessibilityBreakingHStackBackport<Content: View>: View {
	@Environment(\.sizeCategory) private var sizeCategory

	var alignment: VerticalAlignment = .center
	var brokenAlignment: HorizontalAlignment = .center
	var spacing: CGFloat?
	@ViewBuilder var content: Content

	var body: some View {
		if sizeCategory >= .accessibilityMedium {
			VStack {
				content
			}
		} else {
			HStack {
				content
			}
		}
	}
}
