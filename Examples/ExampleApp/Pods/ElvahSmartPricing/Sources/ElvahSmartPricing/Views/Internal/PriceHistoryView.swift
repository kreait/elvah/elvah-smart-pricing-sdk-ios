// Copyright © elvah. All rights reserved.

import SwiftUI

#if canImport(Charts)
	import Charts
#endif

@available(iOS 16.0, *)
struct PriceHistoryView: View {
	@State private var displayMode: DisplayMode = .week

	var priceHistory: LoadableState<PriceHistory>

	var body: some View {
		AccentedCard {
			VStack(spacing: 15) {
				controls
				switch priceHistory {
				case .absent,
				     .loading:
					loadingContent
				case .error:
					EmptyView() // This case should never occur since the view will not be shown
				case let .loaded(priceHistory):
					chart(with: priceHistory)
				}
			}
			.padding()
			.frame(height: 150)
		}
		.transformEffect(.identity)
		.zIndex(1)
		.redacted(reason: isRedacted ? .placeholder : [])
		.animation(.default, value: displayMode)
		.accessibilityElement(children: .contain)
		.accessibilityLabel(localizedAccessibilityLabel)
	}

	@ViewBuilder private var controls: some View {
		HStack {
			Button(moduleTitle: "Week") {
				displayMode = .week
			}
			.bold(displayMode == .week)
			.foregroundColor(displayMode == .week ? .accentedPrimary : .secondary)
			.accessibilityLabel(Text("Show past 7 days"))
			Button(moduleTitle: "Month") {
				displayMode = .month
			}
			.bold(displayMode == .month)
			.foregroundColor(displayMode == .month ? .accentedPrimary : .secondary)
			.accessibilityLabel(Text("Show past 30 days"))
		}
		.frame(maxWidth: .infinity, alignment: .trailing)
		.font(.quicksand(size: 16))
		.allowsHitTesting(isRedacted == false)
	}

	@ViewBuilder private var loadingContent: some View {
		VStack {
			ProgressView()
			Text("Fetching price history", bundle: .automatic)
				.font(.quicksand(size: 16))
				.foregroundColor(.secondary)
				.unredacted()
		}
		.frame(maxHeight: .infinity)
	}

	@ViewBuilder private func chart(with priceHistory: PriceHistory) -> some View {
		let subRange = displayMode.subRange(in: priceHistory)
		Chart {
			ForEach(priceHistory.values) { data in
				LineMark(x: .value("Date", data.date), y: .value("Price", data.price.amount))
					.foregroundStyle(Color.accentedPrimary)
					.interpolationMethod(.catmullRom)
					.lineStyle(.init(lineWidth: 3))
					.accessibilityLabel(localizedDataPointAccessibilityLabel(for: data))
					.accessibilityValue(localizedDataPointAccessibilityValue(for: data))
			}
		}
		.chartXScale(domain: [subRange.start, subRange.end])
		.chartXAxis {
			AxisMarks(
				preset: .aligned,
				values: .automatic(desiredCount: displayMode.visibleXAxisMarkers)
			) { value in
				AxisValueLabel(format: displayMode.xAxisValueLabelFormat)
			}
		}
		.chartYScale(domain: [subRange.lowestPrice.amount, subRange.highestPrice.amount])
		.chartYAxis {
			AxisMarks(values: [subRange.lowestPrice.amount, subRange.highestPrice.amount]) { value in
				AxisGridLine()
				if let price = value.as(Double.self) {
					AxisValueLabel(price.formatted(.currency(code: "EUR")))
						.font(.quicksand(size: 13).bold())
						.foregroundStyle(Color.primary)
				}
			}
		}
	}

	private var isRedacted: Bool {
		priceHistory.isLoaded == false
	}

	// MARK: - Accessibility

	private func localizedDataPointAccessibilityLabel(for data: HistoricPrice) -> Text {
		Text("\(data.date.formatted(.dateTime.day().month(.wide)))", bundle: .automatic)
	}

	private func localizedDataPointAccessibilityValue(for data: HistoricPrice) -> Text {
		Text(" \(data.price.formatted()) per kilo watt hour ", bundle: .automatic)
	}

	private var localizedAccessibilityLabel: Text {
		Text("Price History", bundle: .automatic)
	}
}

@available(iOS 16.0, *)
extension PriceHistoryView {
	enum DisplayMode: Hashable {
		case week
		case month

		func subRange(in priceHistory: PriceHistory) -> PriceHistory.SubRange {
			switch self {
			case .week: priceHistory.pastWeek
			case .month: priceHistory.pastMonth
			}
		}

		var visibleXAxisMarkers: Int {
			switch self {
			case .week: 7
			case .month: 2
			}
		}

		var xAxisValueLabelFormat: Date.FormatStyle {
			switch self {
			case .week: .dateTime.weekday(.narrow)
			case .month: .dateTime.day().month()
			}
		}
	}
}

#Preview("Light") {
	if #available(iOS 16.0, *) {
		VStack {
			PriceHistoryView(priceHistory: .loading)
			PriceHistoryView(priceHistory: .loaded(try! .init(
				values: .pastMonth,
				currentCheapestPrice: .init(0.42)
			)))
		}
		.padding(.horizontal)
		.withFontRegistration()
	} else {
		Text(verbatim: "Please use an iOS 16 or later simulator")
	}
}

#Preview("Dark") {
	if #available(iOS 16.0, *) {
		VStack {
			PriceHistoryView(priceHistory: .loading)
			PriceHistoryView(priceHistory: .loaded(try! .init(
				values: .pastMonth,
				currentCheapestPrice: .init(0.42)
			)))
		}
		.padding(.horizontal)
		.withFontRegistration()
		.preferredColorScheme(.dark)
	} else {
		Text(verbatim: "Please use an iOS 16 or later simulator")
	}
}
