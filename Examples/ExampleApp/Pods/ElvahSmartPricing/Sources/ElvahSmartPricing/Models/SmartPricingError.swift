// Copyright © elvah. All rights reserved.

/// An error type describing common errors that can occur with `SmartPricingView`.
public enum SmartPricingError: Error, Sendable, Hashable {
	/// An error indicating that the fetched pricing data is empty.
	///
	/// Possible causes include:
	/// - Empty evseIds array passed to `SmartPricingView`.
	/// - No data could be found for any of the evseIds passed to `SmartPricingView`.
	case fetchedDataIsEmpty

	/// An error indicating that the fetched pricing history data is invalid.
	///
	/// Possible causes include:
	/// - Insufficient number of data points received
	/// - No price history available for the set of evse ids.
	case fetchedPriceHistoryDataIsInvalid

	/// An unknown error.
	case unknown
}
