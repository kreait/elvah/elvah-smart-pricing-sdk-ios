// Copyright © elvah. All rights reserved.

import Foundation

/// A data point specifying some price at a given date
struct HistoricPrice: Identifiable, Hashable, Sendable {
	/// The identifier of the historic price point.
	///
	/// This is the object's ``ChargepointHistoricPrice/date``.
	var id: Date { date }

	/// The date of the price of the chargepoint.
	var date: Date

	/// The price of the chargepoint at the provided date specified in
	/// ``ChargepointHistoricPrice/date``.
	var price: Currency
}
