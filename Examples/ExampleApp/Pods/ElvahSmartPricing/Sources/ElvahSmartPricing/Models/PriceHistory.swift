// Copyright © elvah. All rights reserved.

import Foundation

/// A container object storing information of historic prices of chargepoints.
struct PriceHistory: Hashable, Sendable {
	/// The prices in the history range.
	var values: [HistoricPrice]

	/// The current cheapest price from all available evse ids.
	var currentCheapestPrice: Currency

	/// The subrange containing pricing information about the past 7 days.
	var pastWeek: SubRange

	/// The subrange containing pricing information about the past 30 days.
	var pastMonth: SubRange

	/// A container object storing information of historic prices of chargepoints.
	/// - Parameters:
	///   - values: The prices in the history range.
	///   - currentCheapestPrice: The current cheapest price from all available evse ids.
	init(values: [HistoricPrice], currentCheapestPrice: Currency) throws {
		self.values = values
		self.currentCheapestPrice = currentCheapestPrice

		pastWeek = try Self.subRange(from: values, pastDays: 7)
		pastMonth = try Self.subRange(from: values, pastDays: 30)
	}
}

extension PriceHistory {
	/// A subrange containing pricing information about the past x days.
	struct SubRange: Hashable, Sendable {
		/// The start of the subrange.
		var start: Date

		/// The start of the subrange.
		var end: Date

		/// The lowest price of any chargepoint in the subrange.
		var lowestPrice: Currency

		/// The highest price of any chargepoint in the subrange.
		var highestPrice: Currency
	}
}

private extension PriceHistory {
	static func priceRange(
		from values: [HistoricPrice]
	) throws -> (lower: Currency, upper: Currency) {
		let prices = values.map(\.price)
		guard let min = prices.min(), let max = prices.max() else {
			throw SmartPricingError.fetchedPriceHistoryDataIsInvalid
		}
		return (min, max)
	}

	static func subRange(from values: [HistoricPrice], pastDays: Int) throws -> SubRange {
		let values = Array(values.suffix(pastDays))
		let priceRange = try Self.priceRange(from: values)

		guard let start = values.first?.date, let end = values.last?.date else {
			throw SmartPricingError.fetchedPriceHistoryDataIsInvalid
		}

		return .init(
			start: start,
			end: end,
			lowestPrice: priceRange.lower,
			highestPrice: priceRange.upper
		)
	}
}
