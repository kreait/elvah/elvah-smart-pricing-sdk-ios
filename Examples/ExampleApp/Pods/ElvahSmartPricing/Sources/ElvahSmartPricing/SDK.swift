// Copyright © elvah. All rights reserved.

import OSLog
import SwiftUI
import UIKit

/// A helper type to determine the bundle that contains the SDK's assets.
///
/// - For Swift packages, this will simply return Bundle.module and
/// - For CocoaPods, this will find the URL of the generated bundle and initialize the bundle with
/// that.
final class SDK {}

// MARK: - Logger

extension SDK {
	/// An internal logger.
	static let logger = Logger(
		subsystem: bundle.bundleIdentifier ?? "elvah.smart-pricing-pricing",
		category: "SDK"
	)
}

// MARK: - Bundle

extension SDK {
	/// The bundle used inside the SDK to access assets.
	static let bundle: Bundle = {
		#if SWIFT_PACKAGE
			.module
		#else
			let sdkBundle = Bundle(for: SDK.self)

			guard let resourceBundleURL = sdkBundle.url(
				forResource: "ElvahSmartPricing",
				withExtension: "bundle"
			) else {
				fatalError("ElvahSmartPricing.bundle not found")
			}

			guard let resourceBundle = Bundle(url: resourceBundleURL) else {
				fatalError("Cannot access ElvahSmartPricing.bundle")
			}

			return resourceBundle
		#endif
	}()
}

// MARK: - Fonts

extension SDK {
	/// A global helper flag indicating if the fonts have already been registered.
	///
	/// This is done to prevent repeated registrations.
	@MainActor static var hasRegisteredFonts = false

	/// Registers all fonts that this SDK uses.
	@MainActor static func registerFonts() {
		guard hasRegisteredFonts == false else {
			return
		}
		for item in SDKFont.allCases {
			registerFont(fileName: item.fileName)
		}
		hasRegisteredFonts = true
	}

	/// Registers an individual font.
	/// - Parameter fileName: The file name of the font in the bundle.
	@MainActor private static func registerFont(fileName: String) {
		guard let pathForResourceString = Bundle.automatic.path(forResource: fileName, ofType: nil)
		else {
			SDK.logger.error("Could not register font »\(fileName)«: File not found.")
			return
		}

		let url = NSURL(fileURLWithPath: pathForResourceString as String)
		var errorRef: Unmanaged<CFError>?
		CTFontManagerRegisterFontsForURL(url, .process, &errorRef)

		if errorRef != nil {
			SDK.logger.error("Could not register font »\(fileName)«: \(errorRef.debugDescription)")
		}
	}
}

// MARK: - Bundle Helper

extension Bundle {
	static let automatic = SDK.bundle
}
