// Copyright © elvah. All rights reserved.

import SwiftUI

extension Font {
	/// Returns the "Kreon" font with the specified size and relative text style.
	static func kreon(size: CGFloat, relativeTo textStyle: Font.TextStyle = .body) -> Font {
		.custom(SDKFont.kreon.rawValue, size: size, relativeTo: textStyle)
	}

	/// Returns the "Quicksand" font with the specified size and relative text style.
	static func quicksand(size: CGFloat, relativeTo textStyle: Font.TextStyle = .body) -> Font {
		.custom(SDKFont.quicksand.rawValue, size: size, relativeTo: textStyle)
	}
}
