// Copyright © elvah. All rights reserved.

import SwiftUI

extension View {
	/// The spacing between items that will be applied to the content.
	func itemSpacing(_ spacing: CGFloat?) -> some View {
		environment(\.itemSpacing, spacing)
	}
}

extension EnvironmentValues {
	/// The spacing between items that will be applied to the content.
	var itemSpacing: CGFloat? {
		get { self[ItemSpacingKey.self] }
		set { self[ItemSpacingKey.self] = newValue }
	}
}

private struct ItemSpacingKey: EnvironmentKey {
	static let defaultValue: CGFloat? = nil
}
