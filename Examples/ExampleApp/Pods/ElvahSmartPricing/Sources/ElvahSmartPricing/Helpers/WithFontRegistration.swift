// Copyright © elvah. All rights reserved.

import SwiftUI

extension View {
	/// Registers the SDK's fonts during the initialization of the view.
	///
	/// - Warning: This in only meant to be used in previews.
	/// - Returns: A view that registers the SDK's fonts during its initialization.
	@MainActor func withFontRegistration() -> some View {
		InitHandlerView {
			SDK.registerFonts()
		} content: {
			self
		}
	}
}

/// A helper view to run some code in the initialization of a view
///
/// This is useful with Previews.
@MainActor
private struct InitHandlerView<Content>: View where Content: View {
	private var content: Content

	init(block: () -> Void, @ViewBuilder content: () -> Content) {
		self.content = content()
		block()
	}

	var body: some View {
		content
	}
}
