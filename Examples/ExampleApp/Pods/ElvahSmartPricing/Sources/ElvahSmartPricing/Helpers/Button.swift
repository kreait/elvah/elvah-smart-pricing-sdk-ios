// Copyright © elvah. All rights reserved.

import SwiftUI

extension Button {
	/// A Button view that uses the `.automatic` bundle for its title.
	/// - Parameters:
	///   - moduleTitle: The title of the button.
	///   - action: The action to perform when the button is tapped.
	init(moduleTitle: LocalizedStringKey, action: @escaping () -> Void) where Label == Text {
		self.init(action: action) {
			Text(moduleTitle, bundle: .automatic)
		}
	}
}
