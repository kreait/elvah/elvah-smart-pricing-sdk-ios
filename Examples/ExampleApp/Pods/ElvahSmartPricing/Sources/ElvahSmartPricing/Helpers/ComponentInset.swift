// Copyright © elvah. All rights reserved.

import SwiftUI

extension View {
	/// The horizontal padding that will be applied to the content.
	func componentInset(_ inset: CGFloat?) -> some View {
		environment(\.componentInset, inset)
	}
}

extension EnvironmentValues {
	/// The horizontal padding that will be applied to the content.
	var componentInset: CGFloat? {
		get { self[ComponentInsetKey.self] }
		set { self[ComponentInsetKey.self] = newValue }
	}
}

private struct ComponentInsetKey: EnvironmentKey {
	static let defaultValue: CGFloat? = nil
}
