// Copyright © elvah. All rights reserved.

import Foundation

extension Date {
	/// A helper function initializing a `Date` that is a specified amount of days in the past,
	/// optionally also setting the `hour` and `minute` value.
	///
	/// This is used to create mock data for Previews and testing.
	static func daysInPast(_ count: Int, hour: Int? = nil, minute: Int? = nil) -> Date {
		let currentDay = Calendar.current.component(.day, from: Date())
		let components = DateComponents(day: currentDay - count, hour: hour, minute: minute)
		return Calendar.current.date(from: components) ?? Date()
	}
}
