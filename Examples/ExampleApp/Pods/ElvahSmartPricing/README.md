elvah Smart Pricing SDK

## Content

1. [Installation](#installation)
	- [Swift Package Manager](#swift-package-manager)
	- [CocoaPods](#cocoapods)
2. [Getting Started](#getting-started)
	- [Safe Area Padding](#safe-area-padding)
	- [Mocking Data](#mocking-data)
2. [License](#license)

## Installation

The elvah Smart Pricing SDK supports iOS 14+ and requires Swift 5.10 to compile. You can install it either through the Swift Package Manager or through CocoaPods.

### Swift Package Manager

Add the following line to the dependencies in your `Package.swift` file:

```swift
.package(url: "https://gitlab.com/kreait/elvah/elvah-smart-pricing-sdk-ios", from: "0.1.0")
```

Alternatively, if you want to add the package to your Xcode project, go to `File` > `Add Packages...` and enter the URL "https://gitlab.com/kreait/elvah/elvah-smart-pricing-sdk-ios" into the search field at the top right. The package should appear in the list. Select it and click "Add Package" in the bottom right.

### CocoaPods

In your `Podfile`, add the following line:

```
pod "ElvahSmartPricing", :git => "https://gitlab.com/kreait/elvah/elvah-smart-pricing-sdk-ios", :tag => '0.1.0'
```

Then run `pod install` and you are good to go!

#### Sandboxing

Depending on your project setup, you might see an error when trying to build the project. It could look like this:

> Error: Sandbox: rsync.samba(95743) deny(1) file-read-data (...)/Frameworks/ElvahSmartPricing.framework/ElvahSmartPricing.bundle

This error is due to Xcode's default sandboxing behavior that must be turned off to use CocoaPods. You can do that by going to your target's build settings and setting `ENABLE_USER_SCRIPT_SANDBOXING` to `NO`. Your project should then build again.

## Getting Started

The elvah Smart Pricing SDK exposes a single SwiftUI view called `SmartPricingView` that takes an array of evse ids as its input:

```swift
SmartPricingView(evseIds: ["DE*test*0001", "DE*test*0002"])
```

> Note: The `SmartPricingView` assumes that it will only be passed valid evse ids of one of the supported CPOs. Failing to do so will cause an error state, i.e. the view will show an error instead of the pricing information.

### Safe Area Padding

The `SmartPricingView` contains an edge-to-edge scrollview, so you should not apply a `.padding()` directly to the view. Due to the lack of the `safeAreaPadding` modifier for scroll views in iOS 14, instead, if you need to apply horizontal padding, you can pass it via an additional parameter:

```swift
SmartPricingView(evseIds: ["DE*test*0001", "DE*test*0002"], safeAreaPadding: 20)
```

The default value is `nil`, which will apply the system default.


### Item Spacing

If you want to specify the spacing between the individual items inside the `SmartPricingView`, you can pass an addition value to the view:

```swift
SmartPricingView(evseIds: ["DE*test*0001", "DE*test*0002"], itemSpacing: 10)
```

The default value is `nil`, which will apply the system default.

### Mocking Data

You can use the `SmartPricingView` in a simulated mode that will use mocked pricing information. To do that, simply add the `withMockedData(simulatesError:)` view modifier to the view.

```swift
SmartPricingView(evseIds: [])
  .withMockedData(simulatesError: false)
```

> Note: In the simulated mode, it doesn't matter which evse ids you pass to the view. They will be replace with a set of mock evse ids.

## License

Copyright (c) 2024 elvah GmbH

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
